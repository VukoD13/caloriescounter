package caloriescounter.activities.profile;

import com.vuko.caloriescounter.activities.profile.ProfilePresenter;
import com.vuko.caloriescounter.activities.settings.Variables;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 */
public class ProfilePresenterTest
{
    private ProfilePresenter presenter;

    @Before
    public void init()
    {
        presenter = new ProfilePresenter();
    }

    @Test
    public void calculateFatPercentagesTests()
    {
        double bf;
        double delta = 0.00;

        Variables.sex = Variables.SEX.MALE;
        Variables.weightUnit = Variables.WEIGHT_UNIT.KILOGRAMS;
        Variables.lengthUnit = Variables.LENGTH_UNIT.CENTIMETERS;

        bf = presenter.calculateFatPercentages(88.2, 92);
        Assert.assertEquals(bf, 18.6, delta);

        /////

        Variables.sex = Variables.SEX.FEMALE;
        Variables.weightUnit = Variables.WEIGHT_UNIT.KILOGRAMS;
        Variables.lengthUnit = Variables.LENGTH_UNIT.CENTIMETERS;

        bf = presenter.calculateFatPercentages(88.2, 92);
        Assert.assertEquals(bf, 29.77, delta);

        /////

        Variables.sex = Variables.SEX.MALE;
        Variables.weightUnit = Variables.WEIGHT_UNIT.KILOGRAMS;
        Variables.lengthUnit = Variables.LENGTH_UNIT.INCHES;

        bf = presenter.calculateFatPercentages(88.2, 36.248);
        Assert.assertEquals(bf, 18.6, delta);

        /////

        Variables.sex = Variables.SEX.FEMALE;
        Variables.weightUnit = Variables.WEIGHT_UNIT.KILOGRAMS;
        Variables.lengthUnit = Variables.LENGTH_UNIT.INCHES;

        bf = presenter.calculateFatPercentages(88.2, 36.248);
        Assert.assertEquals(bf, 29.77, delta);

        /////

        Variables.sex = Variables.SEX.MALE;
        Variables.weightUnit = Variables.WEIGHT_UNIT.POUNDS;
        Variables.lengthUnit = Variables.LENGTH_UNIT.INCHES;

        bf = presenter.calculateFatPercentages(194.45, 36.248);
        Assert.assertEquals(bf, 18.55, delta);

        /////

        Variables.sex = Variables.SEX.FEMALE;
        Variables.weightUnit = Variables.WEIGHT_UNIT.POUNDS;
        Variables.lengthUnit = Variables.LENGTH_UNIT.INCHES;

        bf = presenter.calculateFatPercentages(194.45, 36.248);
        Assert.assertEquals(bf, 29.69, delta);
    }
}
