package caloriescounter.model.domain;

import com.vuko.caloriescounter.model.domain.Measurement;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 */
public class MeasurementTest
{
    @Test
    public void testCalculateFat() throws Exception
    {
        Measurement measurement = new Measurement();

        measurement.setWeight(88.5);
        measurement.setFatPercentages(18.12);
        measurement.calculateFat();
        Assert.assertEquals(measurement.getFat(), 16.04, 0);


        measurement.setWeight(88.5);
        measurement.setFatPercentages(18.13);
        measurement.calculateFat();
        Assert.assertEquals(measurement.getFat(), 16.05, 0);


        measurement.setWeight(88.5);
        measurement.setFatPercentages(18.14);
        measurement.calculateFat();
        Assert.assertEquals(measurement.getFat(), 16.05, 0);


        measurement.setFatPercentages(0);
        measurement.calculateFat();
        Assert.assertEquals(measurement.getFat(), 0, 0);
    }
}
