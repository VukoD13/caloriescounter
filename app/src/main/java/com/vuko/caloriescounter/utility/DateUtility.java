package com.vuko.caloriescounter.utility;

import com.vuko.caloriescounter.activities.settings.Variables;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class DateUtility
{
    public static long getCalendarMidnightTimeInMillis()
    {
        Date d = new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(Variables.endDayHour));
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        //Log.e("xd", Long.toString(c.getTimeInMillis()));
        return c.getTimeInMillis();
    }

    public static String getDateAsString(long date)
    {
        Date d = new Date(date);

        //        if(d == null)
        //        {
        //            return "-";
        //        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        String result = dateFormat.format(d);

        return result;
    }
}


