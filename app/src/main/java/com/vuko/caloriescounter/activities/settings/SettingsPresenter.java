package com.vuko.caloriescounter.activities.settings;

import android.util.Log;

import com.activeandroid.query.Delete;
import com.vuko.caloriescounter.model.domain.CaloriesAmount;
import com.vuko.caloriescounter.model.domain.Measurement;
import com.vuko.caloriescounter.utility.DateUtility;

/**
 *
 */
public class SettingsPresenter
{
    public void deleteCaloriesData()
    {
        try
        {
            new Delete().from(CaloriesAmount.class).where("date != ?", DateUtility.getCalendarMidnightTimeInMillis()).execute();
            new Delete().from(Measurement.class).execute();
        }
        finally
        {
            Log.e("xd", "wyczyszczono baze");
            //todo: view jakis, rx?
        }
    }
}
