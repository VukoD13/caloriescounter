package com.vuko.caloriescounter.activities.calories_history;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.vuko.caloriescounter.R;
import com.vuko.caloriescounter.model.domain.CaloriesAmount;
import com.vuko.caloriescounter.utility.DateUtility;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 *
 */
public class CaloriesHistoryAdapter extends RecyclerView.Adapter<CaloriesHistoryAdapter.ViewHolder>
{
    List<CaloriesAmount> personCaloriesValues;

    public CaloriesHistoryAdapter(List<CaloriesAmount> personCaloriesValues)
    {
        this.personCaloriesValues = personCaloriesValues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.activity_calories_history_card_view, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        CaloriesAmount caloriesAmount = personCaloriesValues.get(position);
        holder.caloriesHistoryDateTextView.setText(DateUtility.getDateAsString(caloriesAmount.getDate()));
        holder.caloriesHistoryValueTextView.setText(Integer.toString(caloriesAmount.getValue()));
        holder.caloriesHistoryIsTrainingDayCheckBox.setChecked(caloriesAmount.isTrainingDay());
    }

    @Override
    public int getItemCount()
    {
        return personCaloriesValues.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        @Bind(R.id.caloriesHistoryDateTextView) TextView caloriesHistoryDateTextView;
        @Bind(R.id.caloriesHistoryValueTextView) TextView caloriesHistoryValueTextView;
        @Bind(R.id.caloriesHistoryIsTrainingDayCheckBox) CheckBox caloriesHistoryIsTrainingDayCheckBox;

        public ViewHolder(View view)
        {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
