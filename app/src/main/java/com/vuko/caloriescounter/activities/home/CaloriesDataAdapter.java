package com.vuko.caloriescounter.activities.home;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.vuko.caloriescounter.R;
import com.vuko.caloriescounter.model.domain.CaloriesData;
import com.vuko.caloriescounter.utility.ViewUtility;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 *
 */
public class CaloriesDataAdapter extends ArrayAdapter<CaloriesData>
{
    private final int layoutResourceId;
    private final List<CaloriesData> data;
    private final Activity activity;

    public CaloriesDataAdapter(Activity activity, int layoutResourceId, List<CaloriesData> data)
    {
        super(activity, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.activity = activity;
    }

    static class ViewHolder
    {
        private int position;

        @Bind(R.id.caloriesValueTextView) TextView caloriesValueTextView;
        @Bind(R.id.editCaloriesRowButton) Button editCaloriesRowButton;
        @Bind(R.id.deleteCaloriesRowButton) Button deleteCaloriesRowButton;

        public ViewHolder(View view)
        {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
        CaloriesData caloriesData = getItem(position);
        ViewHolder viewHolder;

        if (view == null)
        {
            view = LayoutInflater.from(getContext()).inflate(layoutResourceId, parent, false);
            viewHolder = new ViewHolder(view);

            setOnDeleteCaloriesRowButton(viewHolder);
            setOnEditCaloriesRowButton(viewHolder);

            view.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.position = position;
        viewHolder.caloriesValueTextView.setText(Integer.toString(caloriesData.getValue()));

        return view;
    }

    private void setOnDeleteCaloriesRowButton(ViewHolder viewHolder)
    {
        viewHolder.deleteCaloriesRowButton.setOnClickListener(v -> {
            int pos = viewHolder.position;
            if(pos != -1 && pos < data.size())
            {
                data.get(pos).delete();
                data.remove(pos);
            }
            notifyDataSetChanged();
            ((HomeActivity) activity).updateCaloriesAmount();
        });
    }

    private void setOnEditCaloriesRowButton(ViewHolder viewHolder)
    {
        viewHolder.editCaloriesRowButton.setOnClickListener(v ->
        {
            View promptView = LayoutInflater.from(getContext()).inflate(R.layout.edit_calories_prompt, null);
            EditText editCaloriesPromptEditText = (EditText) promptView.findViewById(R.id.editCaloriesPromptEditText);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder.setView(promptView);

            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton(R.string.promptPositiveButton, (dialog, id) ->
                    {
                        int pos = viewHolder.position;
                        String inputText = editCaloriesPromptEditText.getText().toString();

                        if(inputText.length() > 0)
                        {
                            data.get(pos).setValue(Integer.parseInt(inputText));
                            data.get(pos).save();
                            notifyDataSetChanged();
                            ((HomeActivity) activity).updateCaloriesAmount();
                        }
                        ViewUtility.hideSoftKeyboard(editCaloriesPromptEditText, activity);
                    })
                    .setNegativeButton(R.string.promptNegativeButton, (dialog, id) ->
                    {
                        ViewUtility.hideSoftKeyboard(editCaloriesPromptEditText, activity);
                        dialog.cancel();
                    });

            AlertDialog editCaloriesPrompt = alertDialogBuilder.create();
            editCaloriesPrompt.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            editCaloriesPrompt.show();
        });
    }
}
