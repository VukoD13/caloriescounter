package com.vuko.caloriescounter.activities.settings;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.vuko.caloriescounter.R;
import com.vuko.caloriescounter.activities.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SettingsActivity extends BaseActivity
{
    private SettingsPresenter presenter;

    SharedPreferences.Editor editor;

    @Bind(R.id.endDayHoursSpinner) Spinner endDayHoursSpinner;
    @Bind(R.id.clearCaloriesDataButton) Button clearCaloriesDataButton;

    @Bind(R.id.sexGroup) RadioGroup sexGroup;
    @Bind(R.id.lengthGroup) RadioGroup lengthGroup;
    @Bind(R.id.weightGroup) RadioGroup weightGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        presenter = new SettingsPresenter();

        viewInit();

        editor = getSharedPreferences(Variables.strVariables, 0).edit();
    }

    private void viewInit()
    {
        setSpinner();
        setSexGroup();
        setLengthGroup();
        setWeightGroup();

        setClearCaloriesDataButton();
    }

    private AlertDialog getPromptAlertDialog(View promptView)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(promptView.getContext());
        alertDialogBuilder.setView(promptView);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(R.string.areYouSureYes, (dialog, id) -> {
                    presenter.deleteCaloriesData();
                    Toast.makeText(this, this.getString(R.string.onDoneClearCaloriesDataButton), Toast.LENGTH_LONG).show();
                })
                .setNegativeButton(R.string.areYouSureNo, (dialog, id) -> {
                    dialog.cancel();
                });

        return alertDialogBuilder.create();
    }

    private void setClearCaloriesDataButton()
    {
        View promptView = LayoutInflater.from(getBaseContext()).inflate(R.layout.are_you_sure_prompt, null);

        //            Button yesButton = (Button) findViewById(R.id.areYouSureYes);
        //            Button noButton = (Button) findViewById(R.id.areYouSureNo);

        AlertDialog alertDialog = getPromptAlertDialog(promptView);


        clearCaloriesDataButton.setOnClickListener(v -> {
            alertDialog.show();
        });
    }

    private void putIntSharedPreference(String key, int value)
    {
        editor.putInt(key, value);
        editor.commit();
    }

    private void setLengthGroup()
    {
        if(Variables.lengthUnit == Variables.LENGTH_UNIT.CENTIMETERS)
        {
            lengthGroup.check(R.id.settingsCmRadioButton);
        }
        else if(Variables.lengthUnit == Variables.LENGTH_UNIT.INCHES)
        {
            lengthGroup.check(R.id.settingsInchesRadioButton);
        }

        lengthGroup.setOnCheckedChangeListener((group, checkedId) ->
        {
            switch (checkedId)
            {
                case R.id.settingsCmRadioButton:
                {
                    Variables.lengthUnit = Variables.LENGTH_UNIT.CENTIMETERS;
                    putIntSharedPreference(Variables.strLengthUnit, Variables.LENGTH_UNIT.CENTIMETERS.getValue());
                    break;
                }
                case R.id.settingsInchesRadioButton:
                {
                    Variables.lengthUnit = Variables.LENGTH_UNIT.INCHES;
                    putIntSharedPreference(Variables.strLengthUnit, Variables.LENGTH_UNIT.INCHES.getValue());
                    break;
                }
            }
        });
    }

    private void setWeightGroup()
    {
        if(Variables.weightUnit == Variables.WEIGHT_UNIT.KILOGRAMS)
        {
            weightGroup.check(R.id.settingsKgRadioButton);
        }
        else if(Variables.weightUnit == Variables.WEIGHT_UNIT.POUNDS)
        {
            weightGroup.check(R.id.settingsPoundsRadioButton);
        }

        weightGroup.setOnCheckedChangeListener((group, checkedId) ->
        {
            switch (checkedId)
            {
                case R.id.settingsKgRadioButton:
                {
                    Variables.weightUnit = Variables.WEIGHT_UNIT.KILOGRAMS;
                    putIntSharedPreference(Variables.strWeightUnit, Variables.WEIGHT_UNIT.KILOGRAMS.getValue());
                    break;
                }
                case R.id.settingsPoundsRadioButton:
                {
                    Variables.weightUnit = Variables.WEIGHT_UNIT.POUNDS;
                    putIntSharedPreference(Variables.strWeightUnit, Variables.WEIGHT_UNIT.POUNDS.getValue());
                    break;
                }
            }
        });
    }

    private void setSexGroup()
    {
        if(Variables.sex == Variables.SEX.MALE)
        {
            sexGroup.check(R.id.settingsMaleRadioButton);
        }
        else if(Variables.sex == Variables.SEX.FEMALE)
        {
            sexGroup.check(R.id.settingsFemaleRadioButton);
        }

        sexGroup.setOnCheckedChangeListener((group, checkedId) ->
        {
            switch (checkedId)
            {
                case R.id.settingsMaleRadioButton:
                {
                    Variables.sex = Variables.SEX.MALE;
                    putIntSharedPreference(Variables.strSex, Variables.SEX.MALE.getValue());
                    break;
                }
                case R.id.settingsFemaleRadioButton:
                {
                    Variables.sex = Variables.SEX.FEMALE;
                    putIntSharedPreference(Variables.strSex, Variables.SEX.FEMALE.getValue());
                    break;
                }
            }
        });
    }

    private void setEndDayHour(int hour)
    {
        Variables.endDayHour = hour;
        putIntSharedPreference(Variables.strEndDayHour, hour);
    }

    private void setSpinner()
    {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.endDayHoursArray, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDayHoursSpinner.setAdapter(adapter);

        String[] strings = getResources().getStringArray(R.array.endDayHoursArray);

        endDayHoursSpinner.setSelection(Variables.endDayHour + 2);

        endDayHoursSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                int val = Integer.parseInt(strings[position]);
                if(val > Integer.parseInt(strings[strings.length-1]))
                {
                    val = val - 24;
                }
                setEndDayHour(val);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });
    }
}
