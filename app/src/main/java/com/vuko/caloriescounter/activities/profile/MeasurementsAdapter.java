package com.vuko.caloriescounter.activities.profile;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vuko.caloriescounter.R;
import com.vuko.caloriescounter.model.domain.Measurement;
import com.vuko.caloriescounter.utility.DateUtility;

import java.math.BigDecimal;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 *
 */
public class MeasurementsAdapter extends RecyclerView.Adapter<MeasurementsAdapter.ViewHolder>
{
    private List<Measurement> measurements;

    public MeasurementsAdapter(List<Measurement> measurements)
    {
        this.measurements = measurements;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.activity_profile_card_view, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        Measurement measurement = measurements.get(position);
        Measurement before;
        if(position + 1 < measurements.size())
        {
            before = measurements.get(position + 1);
        }
        else
        {
            before = measurement;
        }
        //todo: del log
        Log.e("xd", Integer.toString(position));

        setTextViewValue(holder.weightTextView, measurement.getWeightAsString());
        setTextViewValue(holder.waistTextView, measurement.getWaistAsString());
        setTextViewValue(holder.fatPercentagesTextView, measurement.getFatPercentagesAsString());
        setTextViewValue(holder.fatTextView, measurement.getFatAsString());
        setTextViewValue(holder.dateTextView, DateUtility.getDateAsString(measurement.getDate()));

        setTextViewValue(holder.weightDifTextView, makeStringDif(measurement.getWeight(), before.getWeight()));
        setTextViewValue(holder.waistDifTextView, makeStringDif(measurement.getWaist(), before.getWaist()));
        setTextViewValue(holder.fatPercentagesDifTextView, makeStringDif(measurement.getFatPercentages(), before.getFatPercentages()));
        setTextViewValue(holder.fatDifTextView, makeStringDif(measurement.getFat(), before.getFat()));
    }

    private String makeStringDif(double now, double before)
    {
        StringBuilder sb = new StringBuilder();
        double dif = now - before;
        dif = new BigDecimal(dif).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        sb.append("("); sb.append(dif); sb.append(")");

        return sb.toString();
    }

    private void setTextViewValue(TextView textView, String string)
    {
        if(string.equals("0") || string.equals("0.0") || string.equals("(0.0)"))
        {
            textView.setText("-");
        }
        else
        {
            textView.setText(string);
        }
    }

    @Override
    public int getItemCount()
    {
        return measurements.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        @Bind(R.id.weightTextView) public TextView weightTextView;
        @Bind(R.id.waistTextView) public TextView waistTextView;
        @Bind(R.id.fatPercentagesTextView) public TextView fatPercentagesTextView;
        @Bind(R.id.fatTextView) public TextView fatTextView;
        @Bind(R.id.dateTextView) public TextView dateTextView;

        @Bind(R.id.weightDifTextView) public TextView weightDifTextView;
        @Bind(R.id.waistDifTextView) public TextView waistDifTextView;
        @Bind(R.id.fatPercentagesDifTextView) public TextView fatPercentagesDifTextView;
        @Bind(R.id.fatDifTextView) public TextView fatDifTextView;

        public ViewHolder(View view)
        {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
