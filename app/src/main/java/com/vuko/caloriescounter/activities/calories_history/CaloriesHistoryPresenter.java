package com.vuko.caloriescounter.activities.calories_history;

import android.util.Log;

import com.activeandroid.query.Select;
import com.vuko.caloriescounter.model.domain.CaloriesAmount;
import com.vuko.caloriescounter.utility.DateUtility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *
 */
public class CaloriesHistoryPresenter
{
    private List <CaloriesAmount> personCaloriesValues;

    public CaloriesHistoryPresenter()
    {
        personCaloriesValues = new ArrayList<>();
    }

    public Observable setPersonCaloriesValuesFromDb()
    {
        Observable observable = rx.Observable.create(subscriber ->
        {
            final long START_DATE = DateUtility.getCalendarMidnightTimeInMillis() - TimeUnit.MILLISECONDS.convert(60, TimeUnit.DAYS);
            try
            {
                personCaloriesValues.clear();
                personCaloriesValues.addAll(new Select().from(CaloriesAmount.class).where("date >= ?", START_DATE).orderBy("date DESC").execute());
            }
            finally
            {
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    public List<CaloriesAmount> getPersonCaloriesValues()
    {
        return personCaloriesValues;
    }

    public Observable setAverageCaloriesTextView(int days)
    {
        return Observable.create(subscriber ->
        {
            Map<String, Integer> values = new HashMap();
            final long START_DATE = DateUtility.getCalendarMidnightTimeInMillis() - TimeUnit.MILLISECONDS.convert(days, TimeUnit.DAYS);
            try
            {
                List<CaloriesAmount> list = new Select().from(CaloriesAmount.class).where("date >= ?", START_DATE).orderBy("date DESC").execute();
                int value = 0, tr = 0, ntr = 0, i = 0, itr = 0, intr = 0;
                for(CaloriesAmount ca : list)
                {
                    if(i == 0)
                    {
                        i++;
                        continue;
                    }

                    value+=ca.getValue();
                    if(ca.isTrainingDay())
                    {
                        itr++;
                        tr+=ca.getValue();
                    }
                    else
                    {
                        intr++;
                        ntr+=ca.getValue();
                    }
                }
                value/=(list.size()-1);
                if(itr > 0)
                {
                    tr /= itr;
                }
                if(intr > 0)
                {
                    ntr /= intr;
                }
                values.put("all", value);
                values.put("trainingDays", tr);
                values.put("lazyDays", ntr);
                //todo: log del
                Log.e("xd", Integer.toString(value));
                Log.e("xd", Integer.toString(tr));
                Log.e("xd", Integer.toString(ntr));
            }
            finally
            {
                subscriber.onNext(values);
            }
        }).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread());
    }
}
