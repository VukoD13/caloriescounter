package com.vuko.caloriescounter.activities.home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.vuko.caloriescounter.R;
import com.vuko.caloriescounter.activities.BaseActivity;
import com.vuko.caloriescounter.activities.profile.ProfileActivity;
import com.vuko.caloriescounter.activities.settings.SettingsActivity;
import com.vuko.caloriescounter.activities.settings.Variables;
import com.vuko.caloriescounter.utility.ViewUtility;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscriber;

public class HomeActivity extends BaseActivity
{
    private HomePresenter presenter;

    private SharedPreferences sharedPreferences;

    private CaloriesDataAdapter adapter;

    @Bind(R.id.addCaloriesButton) Button addCaloriesButton;
    @Bind(R.id.caloriesListView) ListView caloriesListView;
    @Bind(R.id.caloriesEditText) EditText caloriesEditText;
    @Bind(R.id.caloriesAmountTextView) TextView caloriesAmountTextView;

    @Bind(R.id.newWeightButton) Button newWeightButton;
    @Bind(R.id.isTrainingDayCheckBox) CheckBox isTrainingDayCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(Variables.strVariables, 0);
        getSettings();

        if(Variables.firstRun)
        {
            firstRun();
        }

        presenter = new HomePresenter();

        viewInit();

        setCurrentCaloriesDataListFromDb();

        setOnNewWeightButton();
    }

    private void firstRun()
    {
        Variables.firstRun = false;
        SharedPreferences.Editor editor = getSharedPreferences(Variables.strVariables, 0).edit();
        editor.putBoolean(Variables.strFirstRun, false);
        editor.commit();
        finish();
        Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void getSettings()
    {
        Variables.firstRun = sharedPreferences.getBoolean(Variables.strFirstRun, true);
        Variables.sex = Variables.SEX.valueOf(sharedPreferences.getInt(Variables.strSex, Variables.SEX.MALE.getValue()));
        Variables.lengthUnit = Variables.LENGTH_UNIT.valueOf(sharedPreferences.getInt(Variables.strLengthUnit, Variables.LENGTH_UNIT.CENTIMETERS.getValue()));
        Variables.weightUnit = Variables.WEIGHT_UNIT.valueOf(sharedPreferences.getInt(Variables.strWeightUnit, Variables.WEIGHT_UNIT.KILOGRAMS.getValue()));
        Variables.endDayHour = sharedPreferences.getInt(Variables.strEndDayHour, 3);
        //todo: log del
        Log.e("xd", Variables.sex.name());
        Log.e("xd", Variables.lengthUnit.name());
        Log.e("xd", Variables.weightUnit.name());
        Log.e("xd", Integer.toString(Variables.endDayHour));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        MenuItem homeActionMenuItem = menu.findItem(R.id.action_home);
        homeActionMenuItem.setVisible(false);

        MenuItem profileActionMenuItem = menu.findItem(R.id.action_profile);
        profileActionMenuItem.setVisible(true);
        profileActionMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        return true;
    }

    public void updateCaloriesAmount()
    {
        caloriesAmountTextView.setText(Integer.toString(presenter.calculateCaloriesAmount()));
    }

    private void setCurrentCaloriesDataListFromDb()
    {
        Subscriber<Object> subscriber = new Subscriber<Object>()
        {
            @Override
            public void onCompleted()
            {
                adapter.notifyDataSetChanged();
                updateCaloriesAmount();
            }

            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
            }

            @Override
            public void onNext(Object object)
            {
            }
        };

        presenter.setCurrentCaloriesDataListFromDb().subscribe(subscriber);
    }

    private void viewInit()
    {
        isTrainingDayCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> presenter.setTrainingDay(isTrainingDayCheckBox.isChecked()));
        isTrainingDayCheckBox.setChecked(presenter.isTrainingDay());

        adapter = new CaloriesDataAdapter(this, R.layout.calories_list_row, presenter.getCaloriesList());
        caloriesListView.setAdapter(adapter);

        updateCaloriesAmount();

        addCaloriesButtonOnClickListener();
    }

    private void addCaloriesButtonOnClickListener()
    {
        addCaloriesButton.setOnClickListener(view -> {
            presenter.addToCaloriesList(caloriesEditText.getText().toString());
            caloriesEditText.setText("");
            adapter.notifyDataSetChanged();

            updateCaloriesAmount();

            ViewUtility.hideSoftKeyboard(view, this);
        });
    }

    private void setOnNewWeightButton()
    {
        newWeightButton.setOnClickListener(v ->
        {
            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("newWeight", true);
            startActivity(intent);
        });
    }
}
