package com.vuko.caloriescounter.activities.settings;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class Variables
{
    public static String strVariables = "settings";

    public static boolean firstRun;
    public static SEX sex;
    public static WEIGHT_UNIT weightUnit;
    public static LENGTH_UNIT lengthUnit;
    public static int endDayHour;

    public static String strFirstRun = "firstRun";
    public static String strSex = "sex";
    public static String strWeightUnit = "weightUnit";
    public static String strLengthUnit = "lengthUnit";
    public static String strEndDayHour = "endDayHour";

    public enum SEX
    {
        MALE(0),
        FEMALE(1);

        private int value;
        private static Map map = new HashMap<>();

        private SEX(int value)
        {
            this.value = value;
        }

        static
        {
            for (SEX s : SEX.values())
            {
                map.put(s.value, s);
            }
        }

        public static SEX valueOf(int s)
        {
            return (SEX) map.get(s);
        }

        public int getValue()
        {
            return value;
        }
    }

    public enum WEIGHT_UNIT
    {
        KILOGRAMS(0),
        POUNDS(1);

        private int value;
        private static Map map = new HashMap<>();

        private WEIGHT_UNIT(int value)
        {
            this.value = value;
        }

        static
        {
            for (WEIGHT_UNIT wu : WEIGHT_UNIT.values())
            {
                map.put(wu.value, wu);
            }
        }

        public static WEIGHT_UNIT valueOf(int wu)
        {
            return (WEIGHT_UNIT) map.get(wu);
        }

        public int getValue()
        {
            return value;
        }
    }

    public enum LENGTH_UNIT
    {
        CENTIMETERS(0),
        INCHES(1);

        private int value;
        private static Map map = new HashMap<>();

        private LENGTH_UNIT(int value)
        {
            this.value = value;
        }

        static
        {
            for (LENGTH_UNIT lu : LENGTH_UNIT.values())
            {
                map.put(lu.value, lu);
            }
        }

        public static LENGTH_UNIT valueOf(int lu)
        {
            return (LENGTH_UNIT) map.get(lu);
        }

        public int getValue()
        {
            return value;
        }
    }
}
