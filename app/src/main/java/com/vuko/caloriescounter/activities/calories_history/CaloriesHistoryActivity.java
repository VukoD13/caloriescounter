package com.vuko.caloriescounter.activities.calories_history;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.vuko.caloriescounter.R;
import com.vuko.caloriescounter.activities.BaseActivity;

import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscriber;

public class CaloriesHistoryActivity extends BaseActivity
{
    private CaloriesHistoryPresenter presenter;

    private CaloriesHistoryAdapter adapter;

    @Bind(R.id.caloriesHistoryCardList) RecyclerView recyclerView;
    @Bind(R.id.averageCaloriesModeSpinner) Spinner averageCaloriesModeSpinner;
    @Bind(R.id.averageCaloriesTextView) TextView averageCaloriesTextView;

    @Bind(R.id.caloriesHistoryTrainingDaysTextView) TextView trainingDaysTextView;
    @Bind(R.id.caloriesHistoryLazyDaysTextView) TextView trainingLazyTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calories_history);
        ButterKnife.bind(this);

        presenter = new CaloriesHistoryPresenter();

        viewInit();

        setPersonCaloriesValuesFromDb();
    }

    private void viewInit()
    {
        setSpinner();
        setUpRecyclerView();
        setRecyclerViewAdapter();
    }

    private void setSpinner()
    {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.averageCaloriesModesArray, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        averageCaloriesModeSpinner.setAdapter(adapter);

        averageCaloriesModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                switch(position)
                {
                    //look for: averageCaloriesModesArray in strings.xml
                    case 0:
                    {
                        setAverageCaloriesTextView(7);
                        break;
                    }
                    case 1:
                    {
                        setAverageCaloriesTextView(30);
                        break;
                    }
                    case 2:
                    {
                        setAverageCaloriesTextView(90);
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });
    }

    private void setAverageCaloriesTextView(int days)
    {
        Subscriber<Map<String, Integer>> subscriber = new Subscriber<Map<String, Integer>>()
        {
            @Override
            public void onCompleted()
            {
            }

            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
            }

            @Override
            public void onNext(Map<String, Integer> map)
            {
                averageCaloriesTextView.setText(Integer.toString(map.get("all")));
                trainingDaysTextView.setText(Integer.toString(map.get("trainingDays")));
                trainingLazyTextView.setText(Integer.toString(map.get("lazyDays")));
            }
        };

        presenter.setAverageCaloriesTextView(days).subscribe(subscriber);
    }

    private void setUpRecyclerView()
    {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void setRecyclerViewAdapter()
    {
        adapter = new CaloriesHistoryAdapter(presenter.getPersonCaloriesValues());
        recyclerView.setAdapter(adapter);
    }

    private void setPersonCaloriesValuesFromDb()
    {
        Subscriber<Object> subscriber = new Subscriber<Object>()
        {
            @Override
            public void onCompleted()
            {
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
            }

            @Override
            public void onNext(Object object)
            {

            }
        };

        presenter.setPersonCaloriesValuesFromDb().subscribe(subscriber);
    }
}
