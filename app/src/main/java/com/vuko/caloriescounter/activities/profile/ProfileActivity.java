package com.vuko.caloriescounter.activities.profile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.vuko.caloriescounter.R;
import com.vuko.caloriescounter.activities.BaseActivity;
import com.vuko.caloriescounter.model.domain.Measurement;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscriber;

public class ProfileActivity extends BaseActivity
{
    private ProfilePresenter presenter;

    private Context context;
    private MeasurementsAdapter adapter;

    @Bind(R.id.profileCardList) RecyclerView recyclerView;

    @Bind(R.id.addNewMeasurementButton) Button addNewMeasurementButton;
    @Bind(R.id.editLastMeasurementButton) Button editLastMeasurementButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        context = this;
        presenter = new ProfilePresenter();

        viewInit();

        setPersonMeasurementsFromDb();

        setOnAddNewMeasurementButton();
        setOnEditLastMeasurementButton();

        if(getIntent().getBooleanExtra("newWeight", false))
        {
            addNewMeasurementButton.performClick();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        MenuItem profileActionMenuItem = menu.findItem(R.id.action_profile);
        profileActionMenuItem.setVisible(false);

        return true;
    }

    private void viewInit()
    {
        setUpRecyclerView();
        setRecyclerViewAdapter();
    }

    private void setUpRecyclerView()
    {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void setRecyclerViewAdapter()
    {
        adapter = new MeasurementsAdapter(presenter.getPersonMeasurements());
        recyclerView.setAdapter(adapter);
    }

    private void setPersonMeasurementsFromDb()
    {
        Subscriber<Object> subscriber = new Subscriber<Object>()
        {
            @Override
            public void onCompleted()
            {
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
            }

            @Override
            public void onNext(Object object)
            {

            }
        };

        presenter.setPersonMeasurementsFromDb().subscribe(subscriber);
    }

    /**
    ////////////////
    Prompt buttons section
    ///////////////
     **/

    private AlertDialog getPromptAlertDialog(View promptView)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(R.string.promptPositiveButton, null)
                .setNegativeButton(R.string.promptNegativeButton, (dialog, id) -> {
                    dialog.cancel();
                });

        return alertDialogBuilder.create();
    }

    private void setEditTextDouble(EditText editText, double d)
    {
        if (d == 0)
        {
            editText.setText("");
        }
        else
        {
            editText.setText(Double.toString(d));
        }
    }

    private void setOnEditLastMeasurementButton()
    {
        View promptView = LayoutInflater.from(getBaseContext()).inflate(R.layout.measurement_prompt, null);

        EditText weightPromptEditText = (EditText) promptView.findViewById(R.id.weightPromptEditText);
        EditText waistPromptEditText = (EditText) promptView.findViewById(R.id.waistPromptEditText);
        EditText fatPercentagesPromptEditText = (EditText) promptView.findViewById(R.id.fatPercentagesPromptEditText);
        Button calculateFatPercentagesPromptButton = (Button) promptView.findViewById(R.id.calculateFatPercentagesPromptButton);

        calculateFatPercentagesPromptButton.setOnClickListener(v ->
        {

            double weight = presenter.getDoubleFromString(weightPromptEditText.getText().toString());
            double waist = presenter.getDoubleFromString(waistPromptEditText.getText().toString());
            double bf = presenter.calculateFatPercentages(weight, waist);

            fatPercentagesPromptEditText.setText(Double.toString(bf));

        });

        AlertDialog alertDialog = getPromptAlertDialog(promptView);

        editLastMeasurementButton.setOnClickListener(v -> {

            if(presenter.getPersonMeasurements().size() == 0)
            {
                return;
            }

            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            alertDialog.show();

            Measurement measurement = presenter.getPersonMeasurements().get(0);

            setEditTextDouble(weightPromptEditText, measurement.getWeight());
            setEditTextDouble(waistPromptEditText, measurement.getWaist());
            setEditTextDouble(fatPercentagesPromptEditText, measurement.getFatPercentages());

            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(view -> {

                String weightString = weightPromptEditText.getText().toString().trim();
                String waistString = waistPromptEditText.getText().toString().trim();
                String fatPercentagesString = fatPercentagesPromptEditText.getText().toString().trim();

                boolean validate = presenter.validateMeasurementStringFromEditText(weightString)
                        && (presenter.validateMeasurementStringFromEditText(waistString) || waistString.length() == 0)
                        && (presenter.validateMeasurementStringFromEditText(fatPercentagesString) || fatPercentagesString.length() == 0);

                if (validate)
                {
                    measurement.setWeight(presenter.getDoubleFromString(weightPromptEditText.getText().toString()));
                    measurement.setWaist(presenter.getDoubleFromString(waistPromptEditText.getText().toString()));
                    measurement.setFatPercentages(presenter.getDoubleFromString(fatPercentagesPromptEditText.getText().toString()));
                    measurement.calculateFat();

                    measurement.save();
                    adapter.notifyItemChanged(0);

                    alertDialog.dismiss();
                }
            });
        });
    }

    private void setOnAddNewMeasurementButton()
    {
        View promptView = LayoutInflater.from(getBaseContext()).inflate(R.layout.measurement_prompt, null);

        EditText weightPromptEditText = (EditText) promptView.findViewById(R.id.weightPromptEditText);
        EditText waistPromptEditText = (EditText) promptView.findViewById(R.id.waistPromptEditText);
        EditText fatPercentagesPromptEditText = (EditText) promptView.findViewById(R.id.fatPercentagesPromptEditText);
        Button calculateFatPercentagesPromptButton = (Button) promptView.findViewById(R.id.calculateFatPercentagesPromptButton);

        calculateFatPercentagesPromptButton.setOnClickListener(v ->
        {

            double weight = presenter.getDoubleFromString(weightPromptEditText.getText().toString());
            double waist = presenter.getDoubleFromString(waistPromptEditText.getText().toString());
            double bf = presenter.calculateFatPercentages(weight, waist);

            fatPercentagesPromptEditText.setText(Double.toString(bf));

        });

        AlertDialog alertDialog = getPromptAlertDialog(promptView);

        addNewMeasurementButton.setOnClickListener(v -> {

            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            alertDialog.show();

            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(view -> {

                String weightString = weightPromptEditText.getText().toString().trim();
                String waistString = waistPromptEditText.getText().toString().trim();
                String fatPercentagesString = fatPercentagesPromptEditText.getText().toString().trim();

                boolean validate = presenter.validateMeasurementStringFromEditText(weightString)
                        && (presenter.validateMeasurementStringFromEditText(waistString) || waistString.length() == 0)
                        && (presenter.validateMeasurementStringFromEditText(fatPercentagesString) || fatPercentagesString.length() == 0);

                if (validate)
                {
                    Measurement measurement = new Measurement();

                    measurement.setWeight(presenter.getDoubleFromString(weightPromptEditText.getText().toString()));
                    measurement.setWaist(presenter.getDoubleFromString(waistPromptEditText.getText().toString()));
                    measurement.setFatPercentages(presenter.getDoubleFromString(fatPercentagesPromptEditText.getText().toString()));
                    measurement.calculateFat();
                    measurement.setDate(new Date());

                    measurement.save();
                    presenter.getPersonMeasurements().add(0, measurement);
                    adapter.notifyDataSetChanged();

                    alertDialog.dismiss();
                }
            });
        });
    }
}
