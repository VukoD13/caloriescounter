package com.vuko.caloriescounter.activities.profile;

import com.activeandroid.query.Select;
import com.vuko.caloriescounter.activities.settings.Variables;
import com.vuko.caloriescounter.model.domain.Measurement;
import com.vuko.caloriescounter.model.domain.Person;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *
 */
public class ProfilePresenter
{
    public ProfilePresenter()
    {
    }

    public double calculateFatPercentages(double weight, double waist)
    {
        double KG_POUNDS_RATIO = 2.2;
        double CM_INCHES_RATIO = 0.394;

        if(Variables.weightUnit == Variables.WEIGHT_UNIT.KILOGRAMS)
        {
            weight *= KG_POUNDS_RATIO;
        }

        if(Variables.lengthUnit == Variables.LENGTH_UNIT.CENTIMETERS)
        {
            waist *= CM_INCHES_RATIO;
        }

        double bf = 100 * ( -98.42 + (4.15 * waist) - (0.082 * weight)) / weight;

        if(Variables.sex == Variables.SEX.FEMALE)
        {
            bf = 100 * ( -76.76 + (4.15 * waist) - (0.082 * weight)) / weight;
        }

        bf = new BigDecimal(bf).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        return bf;
    }

    public List<Measurement> getPersonMeasurements()
    {
        return Person.getInstance().getMeasurements();
    }

    public Observable setPersonMeasurementsFromDb()
    {
        Observable observable = Observable.create(subscriber -> {
            try
            {
                getPersonMeasurements().clear();
                getPersonMeasurements().addAll(
                        new Select()
                        .from(Measurement.class)
                        .execute()
                );
                Collections.reverse(getPersonMeasurements());
            }
            finally
            {
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    public double getDoubleFromString(String string)
    {
        if (string.length() == 0)
        {
            return 0;
        }
        else
        {
            return (Double.parseDouble(string));
        }
    }

    public boolean validateMeasurementStringFromEditText(String input)
    {
        if(input.length() == 0)
        {
            return false;
        }

        input = Double.toString(Double.parseDouble(input)); // .0

        if(input.length() > 10)
        {
            return false;
        }

        return input.matches("\\d+\\.?\\d*");
    }
}
