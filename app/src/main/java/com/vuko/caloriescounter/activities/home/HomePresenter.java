package com.vuko.caloriescounter.activities.home;

import com.activeandroid.query.Select;
import com.vuko.caloriescounter.model.domain.CaloriesData;
import com.vuko.caloriescounter.model.domain.Person;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *
 */
public class HomePresenter
{
    public HomePresenter()
    {
    }

    public void setTrainingDay(boolean bool)
    {
        Person.getInstance().getCaloriesAmount().setTrainingDay(bool);
        Person.getInstance().getCaloriesAmount().save();
    }

    public boolean isTrainingDay()
    {
        return Person.getInstance().getCaloriesAmount().isTrainingDay();
    }

    public Observable setCurrentCaloriesDataListFromDb()
    {
        Observable observable = Observable.create(subscriber -> {
            try
            {
                getCaloriesList().clear();
                getCaloriesList().addAll (
                        new Select()
                            .from(CaloriesData.class)
                            .execute()
                );
            }
            finally
            {
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    public void addToCaloriesList(String caloriesValue)
    {
        if(caloriesValue.length() > 0 && caloriesValue.length() < 6)
        {
            CaloriesData caloriesData = new CaloriesData(Integer.parseInt(caloriesValue));
            getCaloriesList().add(caloriesData);
            caloriesData.save();
        }
    }

    public int calculateCaloriesAmount()
    {
        int amount = 0;
        for(CaloriesData caloriesData : getCaloriesList())
        {
            amount += caloriesData.getValue();
        }
        Person.getInstance().getCaloriesAmount().setValue(amount);
        Person.getInstance().getCaloriesAmount().save();
        return amount;
    }

    public List<CaloriesData> getCaloriesList()
    {
        return Person.getInstance().getCaloriesList();
    }
}
