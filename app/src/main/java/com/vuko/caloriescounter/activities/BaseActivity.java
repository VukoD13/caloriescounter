package com.vuko.caloriescounter.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.vuko.caloriescounter.R;
import com.vuko.caloriescounter.activities.calories_history.CaloriesHistoryActivity;
import com.vuko.caloriescounter.activities.home.HomeActivity;
import com.vuko.caloriescounter.activities.profile.ProfileActivity;
import com.vuko.caloriescounter.activities.settings.SettingsActivity;

/**
 *
 */
public class BaseActivity extends AppCompatActivity
{
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();

        if (menu != null)
        {
            menu.clear();
            inflater.inflate(R.menu.app_menu, menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu)
    {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_profile:
            {
                startNewActivity(ProfileActivity.class);
                break;
            }
            case R.id.action_home:
            {
                startNewActivity(HomeActivity.class);
                break;
            }
            case R.id.action_caloriesHistory:
            {
                startNewActivity(CaloriesHistoryActivity.class);
                break;
            }
            case R.id.action_settings:
            {
                startNewActivity(SettingsActivity.class);
                break;
            }
            default:
            {
                return super.onOptionsItemSelected(item);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void startNewActivity(Class activityClass)
    {
        Intent intent = new Intent(getApplicationContext(), activityClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }
}
