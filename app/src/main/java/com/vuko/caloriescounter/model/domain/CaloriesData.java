package com.vuko.caloriescounter.model.domain;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.vuko.caloriescounter.utility.DateUtility;

/**
 *
 */
@Table(name = "CALORIES_DATA")
public class CaloriesData extends Model
{
    @Column(name = "value") private int value;

    public CaloriesData()
    {
        super();
    }

    public CaloriesData(int value)
    {
        this();
        this.value = value;
    }

    public int getValue()
    {
        return value;
    }

    public void setValue(int value)
    {
        this.value = value;
    }
}
