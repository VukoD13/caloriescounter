package com.vuko.caloriescounter.model.domain;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.vuko.caloriescounter.utility.DateUtility;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Person
{
    private static Person instance = null;

    public static Person getInstance()
    {
        if (instance == null)
        {
            instance = new Person();
        }
        return instance;
    }

    private List<Measurement> measurements;
    private CaloriesAmount caloriesAmount;

    private Person()
    {
        this.measurements = new ArrayList<>();

        this.caloriesAmount = getCaloriesAmountFromDb();

        if(this.caloriesAmount == null)
        {
            this.caloriesAmount = new CaloriesAmount();
            new Delete().from(CaloriesData.class).execute();
        }
    }

    private CaloriesAmount getCaloriesAmountFromDb()
    {
        return new Select()
                .from(CaloriesAmount.class)
                .where("date = ?", DateUtility.getCalendarMidnightTimeInMillis())
                .executeSingle();
    }

    public List<Measurement> getMeasurements()
    {
        return measurements;
    }

    public List<CaloriesData> getCaloriesList()
    {
        return caloriesAmount.getCalories();
    }

    public CaloriesAmount getCaloriesAmount()
    {
        return caloriesAmount;
    }
}
