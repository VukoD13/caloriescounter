package com.vuko.caloriescounter.model.domain;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.vuko.caloriescounter.utility.DateUtility;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Table(name = "CALORIES_AMOUNT")
public class CaloriesAmount extends Model
{
    @Column(name = "value") private int value;
    @Column(name = "date") private long date;
    @Column(name = "training_day") private boolean trainingDay;

    private List<CaloriesData> calories;

    public CaloriesAmount()
    {
        super();
        this.value = 0;
        this.date = DateUtility.getCalendarMidnightTimeInMillis();
        this.calories = new ArrayList<>();
        this.trainingDay = false;
    }

    public List<CaloriesData> getCalories()
    {
        return calories;
    }

    public void setValue(int value)
    {
        this.value = value;
    }

    public boolean isTrainingDay()
    {
        return trainingDay;
    }

    public void setTrainingDay(boolean trainingDay)
    {
        this.trainingDay = trainingDay;
    }

    public long getDate()
    {
        return date;
    }

    public int getValue()
    {
        return value;
    }
}
