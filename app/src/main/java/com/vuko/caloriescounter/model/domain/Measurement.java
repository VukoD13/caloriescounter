package com.vuko.caloriescounter.model.domain;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 */
@Table(name = "MEASUREMENT")
public class Measurement extends Model
{
    @Column(name = "weight") private double weight;
    @Column(name = "waist") private double waist;
    @Column(name = "fat_percentages") private double fatPercentages;
    @Column(name = "fat") private double fat;
    @Column(name = "date") private long date;

    public Measurement()
    {
        super();
    }

    public void calculateFat()
    {
        double value;
        if(fatPercentages <= 0)
        {
            value = 0;
        }
        else
        {
            value = weight * (fatPercentages / 100);
            value = new BigDecimal(value).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        }
        fat = value;
    }

    ////////////////

    public String getWeightAsString()
    {
        return Double.toString(weight);
    }

    public double getWaist()
    {
        return waist;
    }

    public String getWaistAsString()
    {
        return Double.toString(waist);
    }

    public double getFatPercentages()
    {
        return fatPercentages;
    }

    public String getFatPercentagesAsString()
    {
        return Double.toString(fatPercentages);
    }

    public double getFat()
    {
        return fat;
    }

    public String getFatAsString()
    {
        return Double.toString(fat);
    }

    public void setWeight(double weight)
    {
        this.weight = weight;
    }

    public void setWaist(double waist)
    {
        this.waist = waist;
    }

    public void setFatPercentages(double fatPercentages)
    {
        this.fatPercentages = fatPercentages;
    }

    public void setDate(Date date)
    {
        this.date = date.getTime();
    }

    public double getWeight()
    {
        return weight;
    }

    public long getDate()
    {
        return date;
    }
}
